package at.ziegler.cars;

import java.time.LocalDate;

public class CompleteCar {
	public static void main(String[] args) {

		Person pers1 = new Person("Thomas", "Reumiller", LocalDate.of(2000, 01, 01));
		Producer prod1 = new Producer("Audi", "Deutschland", 0.1);
		Engine engine1 = new Engine("Benzin", 150);
		Car c1 = new Car("Black", 150, 10000, 6, prod1, engine1, 60000);
		Car c2 = new Car("Red", 160, 12000, 5, prod1, engine1, 5000);
		
		pers1.addCar(c1);
		pers1.addCar(c2);
		
		
		System.out.println("Preis von beiden Autos: " + pers1.getValueOfCars());
		
		System.out.println("Alter von pers1: " + pers1.getAge());
	}
}
