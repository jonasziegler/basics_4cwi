package at.ziegler.cars;

public class Car {
	public static double consumption = 9.8; //Multipilkator f�r verbrauch
	
	private String color;
	private int maxSpeed;
	private int price;
	private double usage;
	private Producer producer;
	private Engine engine;
	private int mileadge;

	public Car(String color, int maxSpeed, int basicPrice, double usage, Producer producer, Engine engine,
			int mileadge) {
		super();
		this.color = color;
		this.maxSpeed = maxSpeed;
		this.price = basicPrice;
		this.usage = usage;
		this.producer = producer;
		this.engine = engine;
		this.mileadge = mileadge;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public int getMaxSpeed() {
		return maxSpeed;
	}

	public void setMaxSpeed(int maxSpeed) {
		maxSpeed = maxSpeed;
	}

	public int getPrice() {
		return (int) (price * (1 - producer.getDiscount()));
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public double getUsage() {
		if(mileadge > 50000) {
			this.usage =  (this.usage + (this.usage /100 * consumption));
			return usage;
		}
		else {
			return usage;
		}
	}

	public void setUsage(double usage) {
		this.usage = usage;
	}

	public Producer getProducer() {
		return producer;
	}

	public void setProducer(Producer producer) {
		this.producer = producer;
	}

	public Engine getEngine() {
		return engine;
	}

	public void setEngine(Engine engine) {
		this.engine = engine;
	}

	public int getMileadge() {
		return mileadge;
		
	}

	public void setMileadge(int mileadge) {
		this.mileadge = mileadge;
	}

}
