package at.ziegler.cars;

import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Person {
	private String firstName;
	private String lastName;
	private LocalDate birthday;
	private List<Car> cars;

	public Person(String firstName, String lastName, LocalDate birthday) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.birthday = birthday;
		this.cars = new ArrayList<>();
	}

	public int getAge() {
		LocalDate date = LocalDate.now();
		
		return this.birthday.until(date).getYears();
	}
	
	public int getValueOfCars() {
		int value=0;
		for (Car car : this.cars) {
			value = value + car.getPrice();
		}
		return value;
	}

	public void addCar(Car car) {
		this.cars.add(car);
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public LocalDate getBirthday() {
		return birthday;
	}

	public void setBirthday(LocalDate birthday) {
		this.birthday = birthday;
	}

	public List<Car> getCars() {
		return cars;
	}

	public void setCars(List<Car> cars) {
		this.cars = cars;
	}

}
