package at.ziegler.cars;

public class Producer {
	private String name;
	private String origin;
	private double discount;

	public Producer(String name, String origin, double discount) {
		super();
		this.name = name;
		this.origin = origin;
		this.discount = discount; //in Prozent
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public double getDiscount() {
		return discount;
	}

	public void setDiscount(int discount) {
		this.discount = discount;
	}

}
