package at.ziegler.cars;

public class Engine {
	private String fuelType;
	private int power;

	public Engine(String fuelType, int power) {
		super();
		this.fuelType = fuelType;
		this.power = power;
	}

	public String getFuelType() {
		return fuelType;
	}

	public void setFuelType(String fuelType) {
		fuelType = fuelType;
	}

	public int getPower() {
		return power;
	}

	public void setPower(int power) {
		this.power = power;
	}

}
