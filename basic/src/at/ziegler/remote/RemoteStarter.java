package at.ziegler.remote;

public class RemoteStarter {
	public static void main(String[] args) {
		
		Device d1 = new Device("1234a", Device.type.beamer); //crate a new device
		Device d2 = new Device("abcd1", Device.type.rollo);
		
		Remote r1 = new Remote(30,50,200,"A1","Blue");
		//Remote r2 = new Remote(20,65,250,"B2","Red");
		r1.getColor();
		r1.setColor("orange");
		
		//r1.turnOn();
		//r1.turnOff();
		r1.MyColor();
		
		r1.addDevice(d1); //add device d1 to list
		r1.addDevice(d2);
		
		System.out.println(r1.getDevices().get(0).getSerialNumber()); //ausgabe von der SerialNumber des devces mit der Position 0 in der Liste
		
		
	}
}
