package at.ziegler.remote;

import java.util.ArrayList;
import java.util.List;

public class Remote {
	private int length;
	private int width;
	private int weight;
	private String serialNumber;
	private String color;
	private boolean isOn = false;
	private List<Device> devices; //create array list

	

	public Remote(int length, int width, int weight, String serialNumber, String color) {
		super();
		this.length = length;
		this.width = width;
		this.weight = weight;
		this.serialNumber = serialNumber;
		this.color = color;
		this.devices = new ArrayList<>();
	}

	public void addDevice(Device device) { //function to add devices to the list
		this.devices.add(device);
	}
	
	public void turnOn() {
		System.out.println("powering on");
		this.isOn = true;
	}
	
	public void turnOff() {
		System.out.println("powering off");
		this.isOn = false;
	}
	public void MyColor() {
		System.out.println("I have the color " + this.color);
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public List<Device> getDevices() {
		return devices;
	}

	public void setDevices(List<Device> devices) {
		this.devices = devices;
	}


	
	

}

